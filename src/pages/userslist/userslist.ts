import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UserPage } from '../user/user';

import { UsersproviderProvider } from '../../providers/usersprovider/usersprovider';

@Component({
  selector: 'page-userslist',
  templateUrl: 'userslist.html'
})
export class UserslistPage {

	users;

  constructor(public navCtrl: NavController, private usersProvider: UsersproviderProvider) {
  	this.getUsers();
  }

  openUser(user) {
    this.navCtrl.push(UserPage, { user: user});
  }

  getUsers(){
    this.usersProvider.getUsers()
    .then(data => {
      this.users = data;
      console.log(this.users);
      console.log("Get users method")
    });
  }

}
