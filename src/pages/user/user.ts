import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

import { SinglepostPage } from '../singlepost/singlepost';
import { PopoverPage } from '../popover/popover';

import { PostsproviderProvider } from '../../providers/postsprovider/postsprovider';

/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user',
  templateUrl: 'user.html',
})
export class UserPage {

  user:any;
  posts:any;
  postCount:number = 3; //Mostraremos solo 3 post en el perfil del usuario, el resto será cargado con el infinite scroll

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private postsProvider: PostsproviderProvider, private popoverCtrl: PopoverController) {
    this.user = navParams.data.user;
    this.getPosts();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserPage');
  }

  openPost(post) {
    this.navCtrl.push(SinglepostPage, { post: post });
  }

  getPosts(){
    this.postsProvider.getUserPost(this.user.id)
    .then(data => {
      this.posts = data;
      console.log(this.posts);
      console.log("Get posts method")
    });
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.postCount+=3;
      console.log(this.posts);
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

//Popover para boton de edicion o eliminacion del post
  openPopover(ev, post) {

    let popover = this.popoverCtrl.create(PopoverPage, { post: post});

    popover.present({
      ev: ev
    });
  }

}
