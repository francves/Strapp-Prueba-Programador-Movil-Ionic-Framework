import { Component } from '@angular/core';
import { NavController, PopoverController } from 'ionic-angular';

import { SinglepostPage } from '../singlepost/singlepost';
import { UserPage } from '../user/user';
import { AddpostPage } from '../addpost/addpost';
import { PopoverPage } from '../popover/popover';

import { PostsproviderProvider } from '../../providers/postsprovider/postsprovider';
import { UsersproviderProvider } from '../../providers/usersprovider/usersprovider';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts;
  postCount:number = 10;
  users;

  constructor(public navCtrl: NavController, private postsProvider: PostsproviderProvider, 
    private usersProvider: UsersproviderProvider, private popoverCtrl: PopoverController) {
    this.getPosts();
    this.getUsers();
  }

  openPost(post) {
    this.navCtrl.push(SinglepostPage, { post: post });
  }

  openUser(user) {
    this.navCtrl.push(UserPage, { user: user });
  }

  openAddpost(){
    this.navCtrl.push(AddpostPage, { });
  }

  getPosts(){
    this.postsProvider.getPosts()
    .then(data => {
      this.posts = data;
      console.log(this.posts);
      console.log("Get posts method")
    });
  }

  getUsers(){
    this.usersProvider.getUsers()
    .then(data => {
      this.users = data;
      console.log(this.users);
      console.log("Get users method")
    });
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      this.postCount+=10;
      console.log(this.posts);
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 500);
  }

  //Popover para boton de edicion o eliminacion del post
  openPopover(ev, post) {

    let popover = this.popoverCtrl.create(PopoverPage, { post: post});

    popover.present({
      ev: ev
    });
  }

}