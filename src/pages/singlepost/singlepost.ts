import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

import { UserPage } from '../user/user';
import { PopoverPage } from '../popover/popover';

import { UsersproviderProvider } from '../../providers/usersprovider/usersprovider';
import { CommentsproviderProvider } from '../../providers/commentsprovider/commentsprovider';

/**
 * Generated class for the SinglepostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-singlepost',
  templateUrl: 'singlepost.html',
})
export class SinglepostPage {

  post:any;
  user:any = { id: '', name: '', username: ''};
  comments:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private usersProvider: UsersproviderProvider, private commentsProvider: CommentsproviderProvider,
    private popoverCtrl: PopoverController) {
    //Recibiendo datos del post mediante envío de parámetros entre páginas o componentes
    this.post = navParams.data.post;
    this.getUser();
    this.getComments();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SinglepostPage');
  }

  openUser() {
    this.navCtrl.push(UserPage, { user: this.user});
  }

  getUser(){
    this.usersProvider.getUserById(this.post.userId)
    .then(data => {
      this.user = data;
      console.log(this.user);
      console.log("Get user method")
    });
  }

  getComments(){
    this.commentsProvider.getPostComments(this.post.id)
    .then(data => {
      this.comments = data;
      console.log(this.comments);
      console.log("Get comments method")
    });
  }

//Popover para boton de edicion o eliminacion del post
  openPopover(ev, post) {

    let popover = this.popoverCtrl.create(PopoverPage, { post: post});

    popover.present({
      ev: ev
    });
  }

}
