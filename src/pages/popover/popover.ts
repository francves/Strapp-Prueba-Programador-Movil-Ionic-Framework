import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, AlertController } from 'ionic-angular';

import { AddpostPage } from '../addpost/addpost';

import { PostsproviderProvider } from '../../providers/postsprovider/postsprovider';

/**
 * Generated class for the PopoverPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover',
  templateUrl: 'popover.html',
})
export class PopoverPage {

  post:any;
  edit = true;

  constructor(public navParams: NavParams, public navCtrl: NavController,
    private postsProvider: PostsproviderProvider, public viewCtrl: ViewController,
    public toastCtrl: ToastController, public alerCtrl: AlertController){
    this.post = navParams.data.post
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverPage');
  }

  //Abrimos addpost page para aprovechar esta vista ya creada y la usamos para la edicion de lo post
  editPost(){ // Recibimos los datos del post y una variable string que nos indica que se editara un post
    this.navCtrl.push(AddpostPage, { post: this.post, edit: this.edit });
  }

  deletePost(){
    console.log("Delete post method");
    console.log(this.post);
    this.postsProvider.deletePost(this.post).then((result) => {
      console.log(result);
      this.showToast("Post eliminado con éxito!");
    }, (err) => {
      console.log(err);
    });
    this.viewCtrl.dismiss();
  }

  confirmDeletion() {
    console.log("Confirm deletion method");
    let confirm = this.alerCtrl.create({
      title: 'Eliminar post?',
      message: 'Estás seguro que deseas eliminar este post?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Si',
          handler: () => {
            console.log('Agree clicked');
            this.deletePost();
          }
        }
      ]
    });
    confirm.present()
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present(toast);
  }

}
