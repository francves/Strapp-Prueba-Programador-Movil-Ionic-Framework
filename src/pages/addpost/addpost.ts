import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { PostsproviderProvider } from '../../providers/postsprovider/postsprovider';

/**
 * Generated class for the AddpostPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addpost',
  templateUrl: 'addpost.html',
})
export class AddpostPage {

	newpost: any = {};
  post:any;
  edit:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private postsProvider: PostsproviderProvider, public toastCtrl: ToastController) {
    this.post = navParams.data.post;
    this.edit = navParams.data.edit;
    console.log("Post:");
    console.log(this.post);
    console.log("Edit:");
    console.log(this.edit);
    //Si ingresamos a la página desde el botón de editar post llenaremos los campos del 
    //formulario con los datos del post que queremos editar haciendo uso de two way data binding de angular
    if(this.edit){
      this.newpost.id = this.post.id;
      this.newpost.userId = this.post.userId;
      this.newpost.title = this.post.title;
      this.newpost.body = this.post.body;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddpostPage');
  }

  addPost(){
  	console.log("Add post method");
  	console.log(this.newpost);
  	this.postsProvider.addPost(this.newpost).then((result) => {
    	console.log(result);
      this.showToast("Post agreado con éxito!");
  	}, (err) => {
    	console.log(err);
  	});
    this.navCtrl.pop();
  }

  editPost(){
    console.log("Edit post method");
    console.log(this.newpost);
    this.postsProvider.editPost(this.newpost).then((result) => {
      console.log(result);
      this.showToast("Post editado con éxito!");
    }, (err) => {
      console.log(err);
    });
    this.navCtrl.pop();
  }

  showToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present(toast);
  }


}
