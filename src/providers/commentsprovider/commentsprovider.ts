import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CommentsproviderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CommentsproviderProvider {

apiUrl = 'http://jsonplaceholder.typicode.com';

  constructor(public http: HttpClient) {
    console.log('Hello CommentsproviderProvider Provider');
  }

  getPostComments(postId){
  	return new Promise(resolve => {
    this.http.get(this.apiUrl+'/posts/' + postId + '/comments').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
  }

}
