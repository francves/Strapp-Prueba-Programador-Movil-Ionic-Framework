import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the UsersproviderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsersproviderProvider {

apiUrl = 'http://jsonplaceholder.typicode.com';

  constructor(public http: HttpClient) {
    console.log('Hello UsersproviderProvider Provider');
  }

  getUsers(){
  	return new Promise(resolve => {
    this.http.get(this.apiUrl+'/users').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
  }

  getUserById(userId){
  	return new Promise(resolve => {
    this.http.get(this.apiUrl+'/users/'+userId).subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
  }

}
