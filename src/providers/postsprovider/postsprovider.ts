import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
/*
  Generated class for the PostsproviderProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostsproviderProvider {

apiUrl = 'http://jsonplaceholder.typicode.com';

  constructor(public http: HttpClient) {
    console.log('Hello PostsproviderProvider Provider');
  }

  getPosts(){
  	return new Promise(resolve => {
    this.http.get(this.apiUrl+'/posts').subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
  }

  getUserPost(userId){
  	return new Promise(resolve => {
    this.http.get(this.apiUrl+'/posts?userId=' + userId).subscribe(data => {
      resolve(data);
    }, err => {
      console.log(err);
    });
  });
  }

  addPost(newpost){
    return new Promise((resolve, reject) => {
    this.http.post(this.apiUrl+'/posts', newpost)
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
  }

  editPost(post){
    return new Promise((resolve, reject) => {
      this.http.put(this.apiUrl+'/posts/' + post.id, post)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }

  deletePost(post){
    return new Promise((resolve, reject) => {
      this.http.delete(this.apiUrl+'/posts/' + post.id)
        .subscribe(res => {
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });

  }


}
