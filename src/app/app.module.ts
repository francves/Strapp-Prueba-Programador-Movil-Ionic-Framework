import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { UserslistPage } from '../pages/userslist/userslist';
import { HomePage } from '../pages/home/home';
import { PopoverPage } from '../pages/popover/popover';
import { TabsPage } from '../pages/tabs/tabs';
import { SinglepostPage } from '../pages/singlepost/singlepost';
import { UserPage } from '../pages/user/user';
import { AddpostPage } from '../pages/addpost/addpost';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PostsproviderProvider } from '../providers/postsprovider/postsprovider';
import { UsersproviderProvider } from '../providers/usersprovider/usersprovider';
import { CommentsproviderProvider } from '../providers/commentsprovider/commentsprovider';

@NgModule({
  declarations: [
    MyApp,
    UserslistPage,
    HomePage,
    TabsPage,
    SinglepostPage,
    UserPage,
    AddpostPage,
    PopoverPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    UserslistPage,
    HomePage,
    TabsPage,
    SinglepostPage,
    UserPage,
    AddpostPage,
    PopoverPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PostsproviderProvider,
    UsersproviderProvider,
    CommentsproviderProvider
  ]
})
export class AppModule {}
